﻿
using System;
using System.Collections.Generic;
using System.Linq;

using Autour_TVA;


List<float> DemanderTVAUtilisateur()
{
    List<float> tvas = new List<float>();
    const string ARRET_SAISIE = "STOP";
    string valeurSaisie = "";
    bool demandeArret = false;

    do
    {
        Console.WriteLine("Entrez une TVA existante (1,2 - 1,055 - 1,1) ou STOP pour arrêter:");
        valeurSaisie = Console.ReadLine();

        demandeArret = valeurSaisie == ARRET_SAISIE;

        if (float.TryParse(valeurSaisie, out float tva))
        {
            if (!demandeArret && !tvas.Contains(tva) && (tva == 1.2f || tva == 1.055f || tva == 1.1f))
            {
                tvas.Add(tva);
            }
        }

    } while (!demandeArret);

    return tvas;
}

void AfficherTVAsValides(List<float> tvas, AffichageTVA affichageTVA)
{
    Console.WriteLine("TVAs valides saisies :");

    var requete = tvas.Distinct().OrderBy(tva => tva);

    foreach (float tva in requete)
    {
        affichageTVA(tva);
    }
}

void AfficherTVAsSuperieures(List<float> tvas, float seuil, AffichageTVA affichageTVA)
{
    Console.WriteLine("\nTVAs supérieures à {0} :", seuil);

    var requete = tvas.Where(tva => tva > seuil).Distinct().OrderBy(tva => tva);

    foreach (float tva in requete)
    {
        affichageTVA(tva);
    }
}

void AfficherTVAStandard(float tva)
{
    Console.WriteLine("{0} ({1} €)", tva, tva.ToString());
}

void AfficherTVAVert(float tva)
{
    Console.ForegroundColor = ConsoleColor.Green;
    Console.WriteLine("{0} ({1} €)", tva, tva.ToString());
    Console.ResetColor();
}



float DemanderTVASaisie()
{
    float tva;
    Console.WriteLine("Entrez le taux de TVA (en pourcentage) :");
    while (!float.TryParse(Console.ReadLine(), out tva))
    {
        Console.WriteLine("Veuillez entrer un nombre valide pour le taux de TVA :");
    }
    return tva;
}

float DemanderMontantHTSaisie()
{
    float montantHT;
    Console.WriteLine("Entrez le montant HT :");
    while (!float.TryParse(Console.ReadLine(), out montantHT))
    {
        Console.WriteLine("Veuillez entrer un nombre valide pour le montant HT :");
    }
    return montantHT;
}

void CalculerEtAfficherTTC(float montantHT, float tva, string monnaie, AffichageTTC affichageTTC)
{
    float montantTTC = montantHT + (montantHT * tva / 100);
    affichageTTC(montantTTC, monnaie);
}

void AfficherMontantTTC(float montantTTC, string monnaie)
{
    Console.WriteLine("Montant TTC : {0} {1}", montantTTC, monnaie);
}


Console.OutputEncoding = System.Text.Encoding.UTF8;
List<float> tvas = DemanderTVAUtilisateur();
AfficherTVAsValides(tvas, AfficherTVAStandard);
AfficherTVAsSuperieures(tvas, 1.055f, AfficherTVAVert);

float tva = DemanderTVASaisie();
float montantHT = DemanderMontantHTSaisie();
string monnaie = "€";
CalculerEtAfficherTTC(montantHT, tva, monnaie, AfficherMontantTTC);