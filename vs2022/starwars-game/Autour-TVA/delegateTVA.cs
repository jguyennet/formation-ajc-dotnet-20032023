﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Autour_TVA
{
    /// <summary>
    /// Contrat de méthode pour afficher la TVA 
    /// </summary>
    /// <param name="tva"></param>
    delegate void AffichageTVA(float tva);

    /// <summary>
    /// Contrat de méthode pour afficher le TTC avec le symbole de la monnaie
    /// </summary>
    /// <param name="montantTTC"></param>
    /// <param name="monnaie"></param>
    delegate void AffichageTTC(float montantTTC, string monnaie);
}
